package zmeyka;

import java.util.Objects;

public class Coordinates {
        private int row;
        private int col;
        private char ch;

        public Coordinates(int row, int col,char ch) {
            this.row = row;
            this.col = col;
            this.ch=ch;
        }

    public Coordinates(Coordinates coord) {
        this.row = coord.row;
        this.col = coord.col;
        this.ch=coord.ch;
    }

        public char getChar() {
            return this.ch;
        }
        public void setChar(char ch){
            this.ch=ch;
        }
        // Геттеры и сеттеры для доступа к полям
        public int getRow() {
            return this.row;
        }

        public void setRow(int row) {
            this.row = row;
        }

        public int getCol() {
            return this.col;
        }

        public void setCol(int col) {
            this.col = col;
        }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;

        if (obj == null || getClass()!= obj.getClass()) return false;

        Coordinates that = (Coordinates) obj;
        if (this.row == that.row)
            if (this.col == that.col) {
                return true;
            }

      //  return row == that.row && col == that.col;
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(row, col);
    }
        @Override
        public String toString() {
            return "TwoIntegers{" +
                    "Row=" + row +
                    ", Coll=" + col +
                    '}';
        }
}
