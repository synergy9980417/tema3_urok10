package zmeyka;

import java.util.Scanner;

public class Snake {

    public Coordinates[] body; // Массив координат элементов змейки
    int size=0;
    int[] posTurn = new int[3];//возможный НОВЫЙ манёвр в данный момент. по сторонам света/ где 3-й элемент это текущее направление головы змейки
    Scanner scanner=new Scanner(System.in);

    //устанавливается из объекта FieldZ.cheakPoint при новом шаге, при проверке ячейки для шага, меняется если там есть еда
    //если -1 значит наткнулись на препятствие при ходе
    public int newPart=0;
    //задаём временно координаты и символ головы. они поменяются при инициализации поля.
    Coordinates start=new Coordinates(0,0,'>');
    public int hod=0;
    //появляется после получения данных первого хода turn() в котором вызывается whereWeGoNow
    Coordinates nextPoint; //следующая точка для начала змейки полученная после хода
    static public char[] prtSnake={'^','v','<','>'};

    //создание змейки это всегда прямая линия без поворотов
    public Snake(int size, Coordinates start, int hod) throws MySystem.NumberComparisonException {
        this.size=size;
        this.body = new Coordinates[this.size];
        this.start = start;

        this.hod=hod;
//установили координаты головы в 0 элемент
        this.body[0]=this.start;

        if (hod==1){

            if (this.size>this.start.getRow()) System.err.println("стоит уменьшить размер змейки на"+ ((this.size-this.start.getRow())+1));
            //выбросить исключение
            for (int k=1;k<this.size;k++)
               setBodyPart(0,k,this.start.getRow()+k, this.start.getCol());
            }
        else
        if (hod==2){
            if (this.size>this.start.getRow()) System.err.println("стоит уменьшить размер змейки на"+ ((this.size-this.start.getRow())+1));
            //выбросить исключение
            for (int k=1;k<this.size;k++)
                setBodyPart(0,k,this.start.getRow()-k, this.start.getCol());
        } else
        if (hod==3){
            if (this.size>this.start.getCol()) System.err.println("стоит уменьшить размер змейки на"+ ((this.size-this.start.getCol())+1));
            //выбросить исключение


            for (int k=1;k<this.size;k++)
             setBodyPart(0,k,this.start.getRow(), this.start.getCol()+k);
        } else
        if (hod==4){
            if (this.size>this.start.getCol()) System.err.println("стоит уменьшить размер змейки на"+ ((this.size-this.start.getCol())+1));
            //выбросить исключение
            for (int k=1;k<this.size;k++)
             setBodyPart(0,k,this.start.getRow()+1, this.start.getCol()-k);
        }

       // this.body = new int[this.size][2]; // Создаем массив заданного размера для хранения координат
        // координаты такие 0-й элемент это строки Row; 1-й элемент это колонки, столбцы Col

    }

    //upgrade работает только в движении поэтому в нём вызывается поворот/ newPart количество добавляемых частей к телу получаем извне, в методе объекта FieldZ
    //доступные для заполнения ячейки внутри тела змеи добавляются к голове
    public int upgradeSnake(){
        int head=this.whereIndexHead();
        if (this.newPart>0) {
            int newHead = (head+this.newPart);
            Coordinates[] buffSnake = this.body.clone();
            this.body = new Coordinates[(this.body.length + this.newPart)];
            System.arraycopy(buffSnake, 0, this.body, (newHead), buffSnake.length);
            this.newPart=0;
            //возвращаем новый индекс головы
            return newHead;
        }
        return head;

    }

    //nswe (North South West East) 1 2 3 4
    //получаем необходимые все данные в объект змейка для последующего метода move()
    public void turn(int whereindexhead){
        //задаём возможное движение this.posTurn исходя из текущего положения
        this.possible(whereindexhead);
        System.out.println("Ход соответствует сторонам света");
        System.out.println("По ходу движения вам доступны следующие направления:"+this.posTurn[0]+" или "+this.posTurn[1]);
        System.out.println("1 - север/вверх, 2 - юг/вниз, 3 - запад/влево, 4 - восток/вправо");
        System.out.println("В данном положении возможны следующие манёвры, число: "+this.posTurn[0]+" , "+this.posTurn[1]+", или "+this.posTurn[2]+" :");
        int hod;
        try {
            //пытаемся изменить направление
            hod = ((scanner.next()).charAt(0)-48);
                  //  nextInt();
        //если ход некоректен возвращаем текущеее направлние.
            if (this.posTurn[0]!=hod && this.posTurn[1]!=hod)
            hod=this.whereNow(whereindexhead);
            this.hod=hod;
        } catch (Exception e) {
            // Обработка исключения, например, вывод сообщения о неправильном вводе
            hod=this.whereNow(whereindexhead);
            this.hod=hod;
        }
        infoTurn(hod);
        //обновляем переменную nextPoint (здесь мы намеренно передаём локальную переменную. т.к. она нам говорит о смене направления либо нет)

        this.hod=hod;
        this.whereWeGoNow(hod,whereindexhead);

    }

    public void infoTurn(int nswe){
        if (nswe==1) System.out.println("движение вверх");
        if (nswe==2) System.out.println("движение вниз");
        if (nswe==3) System.out.println("движение влево");
        if (nswe==4) System.out.println("движение вправо");
    }

    //возможный манёвр исходя из текущего положения
    public void possible(int whereindexhead){
        int wherenow=this.whereNow(whereindexhead);
        switch (wherenow){
            case (1):
                this.posTurn[0]=3;
                this.posTurn[1]=4;
                break;
            case (2):
                this.posTurn[0]=3;
                this.posTurn[1]=4;
                break;
            case (3):
                this.posTurn[0]=1;
                this.posTurn[1]=2;
                break;
            case (4):
                this.posTurn[0]=1;
                this.posTurn[1]=2;
                break;
        }
        this.posTurn[2]=wherenow;
    }
    //nswe
    //функция определяет по голове и следующему символу куда сейчас смотри змейка
    private int whereNow(int whereindexhead){
        //если на одной строке у головы ряд больше ряда следующей ячейки змейки, то это east, иначе west
        if(this.body[whereindexhead].getRow()==this.body[whereindexhead+1].getRow())
            if (this.body[whereindexhead].getCol()>this.body[this.whereIndexHead()+1].getCol()) return 4;
            else return 3;
        //если на одном ряде, у головы строка больше строки следующей ячейки тела змеи, то это south иначе north
        if(this.body[whereindexhead].getCol()==this.body[whereindexhead+1].getCol())
            if (this.body[whereindexhead].getRow()>this.body[whereindexhead+1].getRow()) return 2;
            else return 1;
        //по идее до этого безусловного ретурна никогда не должны добраться
        System.err.println("невозможно определить текущее направление змейки в методе Snake.whereNow");
        System.out.println("выбрали север");
        return 1;
    }

    //куда мы идём сейчас. устанавливаем координату и символ головы
    public void whereWeGoNow(int hod,int whereIndexHead){
        switch (hod) {
            case (1):
                this.nextPoint=new Coordinates(this.body[whereIndexHead].getRow() - 1, this.body[whereIndexHead].getCol(),this.prtSnake[hod-1]);
                break;
            case (2):
                this.nextPoint=new Coordinates(this.body[whereIndexHead].getRow() + 1, this.body[whereIndexHead].getCol(),this.prtSnake[hod-1]);
                break;
            case (3):
                this.nextPoint = new Coordinates(this.body[whereIndexHead].getRow(), this.body[whereIndexHead].getCol()-1,this.prtSnake[hod-1]);
                break;
            case (4):
                this.nextPoint= new Coordinates(this.body[whereIndexHead].getRow(), this.body[whereIndexHead].getCol() + 1,this.prtSnake[hod-1]);
                break;
            default:
                var ex = new IllegalArgumentException("Ошибка получения нового места положения nextPoint");
                throw ex;
        }
    }


    public void move() throws MySystem.NumberComparisonException, MySystem.EmptyArrayException {
        //движение это перемещение первого элемента на место поворота или продолжения
        //и перемещение всех ячеек змейки на один элемент вперёд

    //    получить информацию о точке для головы
        int whereIndexHead= this.whereIndexHead();
        //сохраняем текущие координаты головы
        Coordinates lastPoint = new Coordinates(this.body[whereIndexHead].getRow(),this.body[whereIndexHead].getCol(),this.getPrtSnake(whereIndexHead,whereIndexHead));


        //передвигаем тело
        //временное хранилище точки тела
        Coordinates buffPoint;

        switch (whereIndexHead){
            case(0):

                this.moveSnakeBodyPoint(whereIndexHead,this.nextPoint);
                //начинаем с +1 т.к. голова whereIndexHead уже сдвинута
                for (int i=whereIndexHead+1;i<this.body.length;i++) {
                    buffPoint=new Coordinates(this.body[i]);
                    this.moveSnakeBodyPoint(i,lastPoint);
                    lastPoint=new Coordinates(buffPoint);
                }
                break;
            default:
                //если голова не в 0 индексе то мы должны двигаться по индексу внутри змеи, а не на поле
                whereIndexHead--;
                this.body[whereIndexHead]=new Coordinates(this.nextPoint);
                break;

        }
//подправляем положения частей змеи, чтобы каждая часть смотрела в правильную сторону после каждого хода
        for (int i=whereIndexHead;i<this.body.length;i++)
            this.body[i].setChar(this.getPrtSnake(whereIndexHead,i));

    }

    //
    public void newSize(int[] coord){

    }

    //входные параметры это индекс куда установить символ из координаты point по новым координатам topoint
public void moveSnakeBodyPoint(int index, Coordinates topoint) throws MySystem.NumberComparisonException {
    if (index<0&&index>this.body.length)
        throw new MySystem.NumberComparisonException("передан индекс за пределами тела змеи " + index + " в Snake.moveSnakeBodyPoint");

    this.body[index] = new Coordinates((topoint.getRow()), topoint.getCol(),topoint.getChar());
    }

    // Установить координаты элемента по индексу
    public void setBodyPart(int whereindexhead,int index, int row, int col) throws MySystem.NumberComparisonException {
        this.body[index]=new Coordinates(row,col,this.getPrtSnake(whereindexhead,index));
    }



    //т.е. если голова начинается не сначала при upgradeSnake нам нужно понимать где она начинается фактически.

    public int whereIndexHead(){
        int index = 0; // Предполагаем, что заполнение не началось
        for (int i = 0; i < this.body.length; i++) {
            if (this.body[i]!= null) { // Проверяем, является ли текущий элемент заполненным (не равным null)
                index=i;
                break;
            }
        }
        return  index;
    }

    public char getPrtSnake(int whereIndexHead, int indexInBody) throws MySystem.NumberComparisonException {
        //если это первый элемент значит и голова на 0
        if (this.body[indexInBody] == null){
           if (whereIndexHead!=0) {
               throw new MySystem.NumberComparisonException("в пустой змее голова должна быть 0 а не" + whereIndexHead);
           }
          return Snake.prtSnake[(hod-1)];
        }

        //если это голова
        if  (whereIndexHead==indexInBody){
            //public char[] prtSnake={'^','v','<','>'};
            if (this.body[indexInBody].getRow()>this.body[indexInBody+1].getRow()) return this.prtSnake[1]; //'v';
            if (this.body[indexInBody].getRow()<this.body[indexInBody+1].getRow()) return this.prtSnake[0]; // '^';
            if (this.body[indexInBody].getCol()<this.body[indexInBody+1].getCol()) return this.prtSnake[2];//'<';
            if (this.body[indexInBody].getCol()>this.body[indexInBody+1].getCol()) return this.prtSnake[3]; //'>';
        } else
        {
            if (this.body[indexInBody].getRow()>this.body[indexInBody-1].getRow()) return this.prtSnake[0];  //'^';
            if (this.body[indexInBody].getRow()<this.body[indexInBody-1].getRow()) return this.prtSnake[1]; //'v';
            if (this.body[indexInBody].getCol()<this.body[indexInBody-1].getCol()) return this.prtSnake[3]; //'>';
            if (this.body[indexInBody].getCol()>this.body[indexInBody-1].getCol()) return this.prtSnake[2]; //'<';

            if (this.body[indexInBody].getRow()==this.body[indexInBody-1].getRow()&&
                    this.body[indexInBody].getCol()>this.body[indexInBody-1].getCol()
            ) return this.prtSnake[3];  //'>';

            if (this.body[indexInBody].getRow()==this.body[indexInBody-1].getRow()&&
                    this.body[indexInBody].getCol()<this.body[indexInBody-1].getCol()
            ) return this.prtSnake[2];  //'<';

            if (this.body[indexInBody].getCol()==this.body[indexInBody-1].getCol()&&
                    this.body[indexInBody].getRow()>this.body[indexInBody-1].getRow()
            ) return this.prtSnake[1]; //'v';

            if (this.body[indexInBody].getCol()==this.body[indexInBody-1].getCol()&&
                    this.body[indexInBody].getRow()<this.body[indexInBody-1].getRow()
            ) return this.prtSnake[0]; //'^';
        }
        //до сюда мы не должны добраться
        //если не удалось вернуть направление части тела получим Z
        System.err.println("Внимание не удалось выяснить направление части тела в Snake.getPrtSnake");
        return 'Z';
    }

    // Получить координаты элемента змеи по индексу
    public Coordinates getBodyPart(int index) throws MySystem.EmptyArrayException {
        if (this.body[index]==null)
        throw new MySystem.EmptyArrayException("По индексу ("+index+") в Массиве Snake.body из Snake.move() нет данных. Snake.getBodyPart");
        return body[index];
    }


}