package zmeyka;

public class MySystem {
    public static class EmptyArrayException extends Exception {
        public EmptyArrayException(String message) {
            super(message);
        }



    }

    public static class  NumberComparisonException extends Exception {
        public NumberComparisonException(String message) {
            super(message);
        }

    }

    public static class NewMyExcepton4_6 extends RuntimeException{
        public NewMyExcepton4_6(){
            super("Неверный ввод от пользователя, тут нужно вводить либо цифру \'0\', либо символ \'X\'");
        }
    }

}
