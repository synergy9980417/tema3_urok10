package zmeyka;

public class GameVictory {
    public static void printVictory() {
        String[] asciiArt = {
                "          _____",
                "         /     \\    ____",
                "        /       \\  /    \\         УРА ПОБЕДА!",
                "       /  ^   ^  \\/      \\",
                "      |  (o) (o)  |       \\     ----------------",
                "      |     <     |        \\   | Язык: Java     |",
                "      |  \\___/   |         )  | Игра: Змейка    |",
                "      \\___________\\________/  | Статус: Победа! |",
                "            |  |  |            ----------------",
                "           /   /   \\",
                "          /   /     \\",
                "         /   /       \\",
                "        /   /         \\",
                "       /___/           \\_____",
                "           \\_______/\\       \\",
                "                    \\________)"
        };

        for (String line : asciiArt) {
            System.out.println(line);
        }
    }



    public static void printSnakeLose() {
        String[] snakeArt = {
                "         ____",
                "        / . .\\",
                "       \\  ---<",
                "        \\  /",
                "  ______/ /",
                " /      \\/",
                "/",
                "\\             __",
                " \\___________/ V`-._",
                "  `    V V V'     /",
                "   `   V  V'    __/",
                "        VV     /",
        };

        String victoryMessage = "Это провал! пробуем заново!";

        for (String line : snakeArt) {
            System.out.println(line);
        }

        System.out.println();
        System.err.println(victoryMessage);
    }


}
