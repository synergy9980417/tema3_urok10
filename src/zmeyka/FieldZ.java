package zmeyka;

import java.util.Random;

public class FieldZ {
    private Coordinates[] field;
    int size=20;
    int SnakeSize=5;
    int eat=5;//изначальное количество еды

    Coordinates[] eatCoords = new Coordinates[eat];
    Coordinates[] wallCoords;// = new Coordinates[(this.size*this.size)]; //с запасом координаты
    Snake snake;
    char[] eatChar = {'E','A','T'};


    //заносим в массив данных координаты стены, и добавляем на поле
    private void addWall(Coordinates coord){
        if (this.wallCoords == null || this.wallCoords.length == 0) {
            // Если массив пуст, создаем новый массив с одним элементом
            this.wallCoords = new Coordinates[]{coord};}
        else {
            // Создаем временный буфер для копирования текущих элементов массива
            Coordinates[] buff = this.wallCoords.clone();

            // Увеличиваем размерность исходного массива на один элемент
            this.wallCoords = new Coordinates[buff.length + 1];

            // Копируем элементы из временного буфера в начало нового массива
            System.arraycopy(buff, 0, this.wallCoords, 0, buff.length);

            // Добавляем новый элемент в конец массива  //так как количество элементов начинается с 1 а индекс в массиве с 0.
            this.wallCoords[buff.length] = coord; // Исправлено здесь
        }

    }

    public void initField() throws MySystem.NumberComparisonException {
        this.field = new Coordinates[this.size*this.size];
        int next = 0;

        //заполняем поле бортами и пустыми символами
        for (int row=0; row<this.size; row++) {
            for (int col = 0; col < this.size; col++) {
                if (row == 0||(row == (this.size - 1))|| (col == 0)|| (col == (this.size - 1))) {
                    this.addWall(new Coordinates(row,col,'+'));
                    this.field[next++] = new Coordinates(row,col,'+');
                }

                else
                    this.field[next++] = new Coordinates(row,col,' ');
            }
        }

        Random random = new Random();
        //при появлении выбираем рандомно первое направление змейки
        int hod = random.nextInt(4)+1; //так работает рандом чтобы получить от 1 до 4 нужно ставить 5
        //середина поля
        Coordinates start = new Coordinates(this.size/2,this.size/2,Snake.prtSnake[(hod-1)]);

        this.putSnakeOnField(start,hod,0);
        this.putEatOnField(true,this.eat);
        System.out.println("Поле проинициализировано");
    }

    //если first, то не учитываем существующие элементы в массиве. а если не first расширяем существующий массив не затирая элементы которые в нём остались
    public void putEatOnField(Boolean first, int newCount) throws MySystem.NumberComparisonException {
        int i=0;
        if (first) {
            while (i < newCount) {
                Coordinates tempCoord = getRandPoint();
                //если ячейка доступна кладём туда еду
                if (this.isPlaceFree(tempCoord.getRow(), tempCoord.getCol())) {

                    Random random = new Random();

                    int indexArr = random.nextInt(this.eatChar.length);
                    this.eatCoords[i] = new Coordinates(tempCoord.getRow(), tempCoord.getCol(), this.eatChar[indexArr]);

                    //this.field[this.eatCoords[i].getRow()][this.eatCoords[i].getCol()]=this.eatChar[indexArr];
                    //  this.eatCoords[i]
                    i++;
                }
            }
        }
        //это условие на будущее когда нужно будет добавлять еду в процессе игры, а не только при инициализации
        else {
            // Создаем временный буфер для копирования текущих элементов массива
            Coordinates[] buff = this.eatCoords.clone();
            // Увеличиваем размерность исходного массива на newCount
            this.eatCoords = new Coordinates[buff.length + newCount];
            // Копируем элементы из временного буфера в начало нового массива
            System.arraycopy(buff, 0, this.eatCoords, 0, buff.length);
            for (int index=(buff.length-1);index<this.eatCoords.length;index++){
                Coordinates tempCoord=getRandPoint();
                //если ячейка доступна кладём туда еду
                if (this.isPlaceFree( tempCoord.getRow(), tempCoord.getCol())){

                    Random random = new Random();
                    int indexArr = random.nextInt(this.eatChar.length);

                    this.eatCoords[index]=new Coordinates(tempCoord.getRow(),tempCoord.getCol(),this.eatChar[indexArr]);
//                    this.field[this.eatCoords[index].getRow()][this.eatCoords[index].getCol()]=this.eatChar[indexArr];
                    i++;
                }
            }

        }

    this.fillEatOnField();
    }
    public void putSnakeOnField(Coordinates start,int hod,int whereIndexHead) throws MySystem.NumberComparisonException {
         this.snake=new Snake(SnakeSize,start,hod);
         this.fillSnakeOnField(whereIndexHead);
    }

    private void fillSnakeOnField(int whereIndexHead) throws MySystem.NumberComparisonException {
        for(int i=whereIndexHead;i<this.snake.size;i++) {
            this.setCharInCoordOnField(this.snake.body[i], this.snake.body[i].getChar()); // getPrtSnake(whereIndexHead,i)
        }
        this.clearOldCharsOnField();
    }

    public void fillEatOnField() throws MySystem.NumberComparisonException {
        for(int i=0;i<this.eatCoords.length;i++) {
            this.setCharInCoordOnField(this.field[this.calculateIndex(this.eatCoords[i])], this.eatCoords[i].getChar()); // getPrtSnake(whereIndexHead,i)
        }
        this.clearOldCharsOnField();
    }
    //удалить старые части змеи которые уже не актуальны на поле, еду, и. стены если надо
    public void clearOldCharsOnField(){
        for (int row=0; row<this.size; row++) {
            for (int col = 0; col < this.size; col++) {
                //временная координата для проверки
                Coordinates tNew=new Coordinates(row,col,this.getCharFromRowCol(row,col));
                if (!isItInHere(tNew,this.snake.body)&&!isItInHere(tNew,this.eatCoords)&&!isItInHere(tNew,this.wallCoords))
                   clearPointOnField(tNew);
            }
    }

    }
    public char getCharFromRowCol(int row, int col){
        return this.field[this.calculateIndex(row,col)].getChar();
    }

    public char getCharFromRowCol(Coordinates coord){
        return this.field[this.calculateIndex(coord.getRow(),coord.getCol())].getChar();
    }

    //получить индекс массива по координате
    public int calculateIndex(Coordinates coord) {
        int index = (int)(coord.getRow() * this.size + coord.getCol());
        return index;
    }

    //получить индекс массива по координатам
    public int calculateIndex(int row,int col) {
        int index = (int)(row * this.size + col);
        return index;
    }


    public void setCharInCoordOnField(Coordinates coord, char ch){
        this.field[this.calculateIndex(coord)].setChar(ch);
//        coord.setChar(ch);
    }

    //метод проверяет являются ли переданные координаты частью указанного массива
    private boolean isItInHere(Coordinates coord, Coordinates[] arrCoords){
        for (int i = 0; i < arrCoords.length; i++) {
            if (arrCoords[i]==null)continue;
            if (arrCoords[i].equals(coord)) {
                return true;
            }
        }
        return false;
    }


    private Coordinates[] removeСoordFromArray(Coordinates coordToRemove,Coordinates[] arrayCoord) throws MySystem.EmptyArrayException {
        // Проверяем, пуст ли исходный массив
        if (arrayCoord == null || arrayCoord.length == 0) {
            throw new MySystem.EmptyArrayException("Массив передаваемый в метод FieldZ.removeCoordFromArray пуст");
        }

        int indexToRemove = -1;
        for (int i = 0; i < arrayCoord.length; i++) {
            if (arrayCoord[i].equals(coordToRemove)) {
                indexToRemove = i;
                break;
            }
        }

        if (indexToRemove!= -1) {
            // Создаем временный буфер для копирования части массива, кроме удаляемого элемента
            Coordinates[] buff = new Coordinates[arrayCoord.length - 1];

            // Копируем элементы из временного буфера в начало нового массива, пропуская удаляемый элемент
            System.arraycopy(arrayCoord, 0, buff, 0, indexToRemove);
            System.arraycopy(arrayCoord, indexToRemove + 1, buff, indexToRemove, arrayCoord.length - indexToRemove - 1);

            // Переназначаем исходный массив новым, меньшим массивом
            arrayCoord = buff;
        } else {
            throw new IllegalArgumentException("Элемент " + coordToRemove + " не найден в массиве wallCoords");
        }
        return arrayCoord;
    }


    //возвращает массу еды, прибавления в зависимости от символа
    private int massEat(Coordinates coord){
        try {
            if  (this.getCharFromRowCol(coord)==this.eatChar[0])
                  return 1;
            if  (this.getCharFromRowCol(coord)==this.eatChar[1])
                    return 2;
            if  (this.getCharFromRowCol(coord)==this.eatChar[2])
                    return 3;
        } catch (IllegalStateException e) {
            throw new RuntimeException(e);
        } finally {
        }
        return 0;
    }

    //вывод этой функции стоит добавить на изменение параметра добавляемой части при агрейде змейки. без еды змейка не меняется 0
    //с едой меняется. если попадает на стену получаем -1;
    //если есть изменения на поле в результате такого хода то возвращаем true. если ход ведёт к столкновению или ничего не меняет false;
    public boolean cheakPoint(Coordinates cord) throws MySystem.EmptyArrayException {
        //на будущее в этом методе можно добавить разную еду с разным весом у этой вес 1 ячейка
//        if (this.field[cord.getRow()][cord.getCol()]=='E')

            if(this.isItInHere(cord,this.eatCoords)) {
                this.snake.newPart += this.massEat(cord);
                //а также нужно удалить из массива будет этот элемент т.к. мы его съели
                this.eatCoords=this.removeСoordFromArray(cord, this.eatCoords);
                this.eat=this.eatCoords.length;
                return true;
            } else
                // это стена
//        if (this.field[cord.getRow()][cord.getCol()]=='+')

            if(this.isItInHere(cord,this.wallCoords)){
            this.snake.newPart=-1;
                System.err.println("столкновение со стеной!");
            }

            //это тело змеи
       else if(this.isItInHere(cord,this.snake.body)){
                this.snake.newPart=-1;
                System.err.println("змея укусила сама себя");
       }
       return false;
    }



    //    получить случайную точку на поле //внутри стен
    public Coordinates getRandPoint() {
        int min = 1;
        int max = this.size-1;
        Random random = new Random();
        int row = random.nextInt(max - min) + min;
        int col = random.nextInt(max - min) + min;
        return new Coordinates(row,col,this.getCharFromRowCol(row,col));
    }

//    boolean isWall(int row,int col){
//        if (this.getCharFromRowCol(row,col)=='+') return true;
//        return false;
//    }

    boolean isWall(Coordinates coord){
        return isItInHere(coord,this.wallCoords);
    }

    public void printField(int whereindexhead) throws MySystem.NumberComparisonException {
        //обновляем змейку на поле. она всегда меняется
        this.fillSnakeOnField(whereindexhead);


        System.out.printf(" \t");
        for (int row=1;row<size+1;row++)
            System.out.printf(row+"\t");
        System.out.println();

        for(int row=0;row<size;row++){
            System.out.print((row+1)+"\t");
            for (int col=0;col<size;col++) System.out.print("["+this.field[this.calculateIndex(row,col)].getChar()+"]\t");
            System.out.println("");
        }
    }

    private void clearPointOnField(Coordinates coord){
        this.setCharInCoordOnField(coord, ' ');
    }

    boolean isPlaceFree(int rowIndex, int colIndex){
        if (rowIndex>=0||rowIndex<size||colIndex>=0||colIndex<size)
            return (this.field[this.calculateIndex(rowIndex,colIndex)].getChar()==' ');
        return false;
    }

    void setValue(int rowIndex,int colIndex,char Who){
        this.field[this.calculateIndex(rowIndex,colIndex)].setChar(Who);
    }

    boolean isGameOver(char WhoMadeLastTurn) {

        return false;
    }
}
