package zmeyka;
//Напишите “змейку”. Есть поле 20х20, есть змейка длинной, пусть, 3
//        квадрата. Выводится поле с положением змейки. Пользователь вводит, куда
//        сделать следующий шаг
//        -
//        повернуть, или не двигаться. Рисуется поле с
//        новым положением змейки.
//        3.
//        Доработайте змейку, что
//        б при врезании в стену(край поля)
//        -
//        игра
//        оканчивалась
//        4.
//        Доработайте змейку, что б на поле были фрукты. Если змейка их съедает
//        -
//        она растет. Если съела все фрукты
//        -
//        игрок победил.
//        5.
//        Доработайте змейку, пусть на поле еще могут быть стены. в них тоже нельзя
//        врез
//        аться
import java.util.Scanner;
//змейка появляется изначально всегда маленькая. как вначале игры также и после столкновения с препятствием.


public class Zgame {
    FieldZ gamezfield;
    Scanner scanner = new Scanner(System.in);
    int livePlayer=1;

    public void SetupNewGame() throws MySystem.NumberComparisonException {
        System.out.println("новая игра в змейку");
        this.gamezfield=new FieldZ();
        this.gamezfield.initField();

    }
    public byte playZ() throws MySystem.EmptyArrayException, MySystem.NumberComparisonException {
       this.SetupNewGame();

        while (this.livePlayer>0){

            int whereindexhead=this.gamezfield.snake.whereIndexHead();

            this.gamezfield.printField(whereindexhead);
            this.gamezfield.snake.turn(whereindexhead);

            //проверка на препятствие при ходе
            if (this.gamezfield.cheakPoint(this.gamezfield.snake.nextPoint)){
                this.gamezfield.snake.upgradeSnake();
                this.gamezfield.clearOldCharsOnField();}
            //результат проверки в newPart
            if (this.gamezfield.snake.newPart==-1)
            {
                this.livePlayer--;
                //обнулять результаты запускать заново игровое поле
                GameVictory.printSnakeLose();
                return -1;
            }
            else
                //печатать новое положение на поле
                this.gamezfield.snake.move();
                this.gamezfield.clearOldCharsOnField();
                if (this.gamezfield.eatCoords.length==0){
                    GameVictory.printVictory();
                    break;
                }
        }
        return 0;
    }
}
