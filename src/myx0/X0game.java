package myx0;

import zmeyka.MySystem;

import java.util.Calendar;
import java.util.Scanner;

public class X0game {
    FieldX0 gameField;
    Scanner scanner = new Scanner(System.in);
    char whoMakeNextTurn;
    boolean gameOver=false;

    void setupNewGame(){
        System.out.println("новая игра началась");
        this.gameField = new FieldX0();
        this.gameField.initField();
    }

    public void play(){
        this.setupNewGame();
        System.out.println("введите кто ходит первый:");
        char first=this.scanner.next().charAt(0);
        if ((Character.toUpperCase(first)=='X' || (first == '0'))){
           this.whoMakeNextTurn = Character.toUpperCase(first);
        } else {
        //    System.out.println("неизвестный символ, нужно вводить либо X либо 0(ноль). Поэтому первым будет X");
        //    this.whoMakeNextTurn='X';
            throw new MySystem.NewMyExcepton4_6();
        }
        this.gameField.printField();

        while (!gameOver){
            turn();
            this.gameOver=this.gameField.isGameOver(this.whoMakeNextTurn);
            if (this.whoMakeNextTurn=='X'){
                this.whoMakeNextTurn='0';
            } else this.whoMakeNextTurn='X';

        }
    }

    void turn(){
        System.out.println(this.whoMakeNextTurn+", твой ход.");
        System.out.println("Введите строку");
        int rowNumber=this.scanner.nextInt();
        System.out.println("Введите столбец");
        int colNumber=this.scanner.nextInt();
        int rowIndex=rowNumber-1;
        int colIndex=colNumber-1;
        if (this.gameField.isPlaceFree(rowIndex,colIndex)){
            //..
            this.gameField.setValue(rowIndex,colIndex,whoMakeNextTurn);
            this.gameField.printField();
        } else {
            System.out.println("это поле занято. поэтому введите другой ход");
            turn();
        }
    }


}
