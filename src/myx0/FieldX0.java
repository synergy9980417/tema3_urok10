package myx0;

public class FieldX0 {
    public char[][] field;
    int size=3;

    public void initField(){
        this.field = new char[size][size];
        for (int row=0; row<size; row++)
            for (int col=0;col<size;col++) field[row][col]=' ';
        System.out.println("Поля проинициализированы");
    }

public void printField(){
        System.out.printf(" \t");
        for (int row=1;row<size+1;row++)
            System.out.printf(row+"\t");
        System.out.println();

        for(int row=0;row<size;row++){
            System.out.print((row+1)+"\t");
            for (int col=0;col<size;col++) System.out.print("["+this.field[row][col]+"]\t");
            System.out.println("");
        }
}

    boolean isPlaceFree(int rowIndex, int colIndex){
        if (rowIndex>=0||rowIndex<size||colIndex>=0||colIndex<size)
       return (this.field[rowIndex][colIndex]==' ');
    return false;
    }

    void setValue(int rowIndex,int colIndex,char Who){
    this.field[rowIndex][colIndex]=Who;
    }

    boolean isGameOver(char WhoMadeLastTurn) {
//        if (this.field[0][0])
        int countToWin;
        for (int row = 0; row < this.size; row++) {
            countToWin = 0;
            for (int col = 0; col < this.size; col++)
                if (this.field[row][col] == WhoMadeLastTurn) countToWin++;
            if (countToWin == this.size) {
                System.out.println("Игра окончена.  Победил " + WhoMadeLastTurn);
                return true;
            } else continue;
        }

        //проверяем столбцы

        for (int col = 0; col < this.size; col++) {
            countToWin = 0;
            for (int row = 0; row < this.size; row++)
                if (this.field[row][col] == WhoMadeLastTurn) countToWin++;
            if (countToWin == this.size) {

                System.out.println("Игра окончена.  Победил " + WhoMadeLastTurn);
                return true;
            } else continue;
        }

        // Проход по главной диагонали (слева направо)
        countToWin = 0;
        for (int i = 0; i < this.size; i++) {
            if (this.field[i][i] == WhoMadeLastTurn) countToWin++;
        }
        if (countToWin == this.size) {
            System.out.println("Игра окончена.  Победил " + WhoMadeLastTurn);
            return true;
        }
// Проход по побочной диагонали (справа налево)
        countToWin = 0;
        for (int i = 0; i < this.size; i++) {
            if (this.field[i][this.size - 1 - i] == WhoMadeLastTurn) countToWin++;
        }
        if (countToWin == this.size) {
            System.out.println("Игра окончена.  Победил " + WhoMadeLastTurn);
            return true;
        }
        return false;
    }
}
